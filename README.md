# React app generator

React app generator is a small Bash script that generates react app templates.

## Usage

From terminal:

```bash
bash script.sh
```

From UI just doble click on script.sh and wait.
