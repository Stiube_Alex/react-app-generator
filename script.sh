#!bin/bash

echo "==================== START ===================="

app_name="app"

# Generate directories
mkdir $app_name
mkdir $app_name/public
mkdir $app_name/src

# Load assets
gitignore=$(< ./assets/.gitignore)
index=$(cat  "./assets/.html")
readme=$(sed -i "s/==app_name==/$app_name/g" ./assets/.md)
robots=$(< ./assets/robots.txt)
prettier=$(< ./assets/.prettierrc)
css=$(< ./assets/.css)
react=$(< ./assets/index.js)
component=$(< ./assets/.jsx)

# Initialize packages
cd $app_name
echo "{
\"name\": \"$app_name\"," > ./package.json

echo '"version": "0.1.0",
  "private": true,
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
  "eslintConfig": {
    "extends": [
      "react-app",
      "react-app/jest"
    ]
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  }
}
' >> ./package.json
npm install react react-dom react-scripts
npm audit fix

# Initialize git
git init -b main
echo $gitignore > .gitignore

# Generate Readme file
echo $readme > ./README.md

# Generate prettier file
echo $prettier > ./.prettierrc

# Generate index.html file
echo $index > ./public/index.html

# Generate robots.txt file
echo $robots > ./public/robots.txt

# Generate favicon
curl "https://cdn-icons.flaticon.com/png/512/4543/premium/4543531.png?token=exp=1657968442~hmac=01b908bc61eb4719fb7dc5a76d008815" --output ./public/favicon.ico

# Initialize React index
echo $css > ./src/index.css
echo $component > ./src/App.jsx
echo $react > ./src/index.js

# Format all files
npm install --global prettier
prettier --write .

echo "==================== DONE ===================="

# Run the application
# npm start


